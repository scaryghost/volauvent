include $(VOLAUVENT_HOME)/common.mk

# module, app name, deps, inc dirs
define define_static_lib_rules =
$(1)_CXXFLAGS += -fPIC

$$(eval $$(call define_common_rules,$(1),lib$(2).a,$(4)))

$$($(1)_APP_OUTPUT): $$($(1)_OBJS) $(3) | $$($(1)_OUTPUT_DIR)
	$(AR) -cvruUP $$@ $$?
endef