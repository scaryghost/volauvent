include $(VOLAUVENT_HOME)/common.mk

# module, app name, deps
define define_dynamic_lib_rules =
$(1)_SO_NAME := lib$(2).so
$(1)_SHORT_NAME := $$($(1)_SO_NAME).$$(VERSION_MAJOR)
$(1)_FULL_APP_NAME := $$($(1)_SO_NAME).$$(VERSION)

$(1)_CXXFLAGS += -fPIC

$$(eval $$(call define_common_rules,$(1),))

$$($(1)_APP_OUTPUT): $$($(1)_OBJS $(3) | $$($(1)_OUTPUT_DIR)
	$(CXX) -o $$@ $$(LD_FLAGS) -shared -Wl,--soname,$$($(1)_SHORT_NAME) $$^
	ln -sf $$($(1)_FULL_APP_NAME) $$($(1)_OUTPUT_DIR)/$$($(1)_SHORT_NAME)
	ln -sf $$($(1)_SHORT_NAME) $$($(1)_OUTPUT_DIR)/$$($(1)_SO_NAME)
endef
