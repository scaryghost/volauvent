ifndef VOLAUVENT_HOME
    $(error required variable "VOLAUVENT_HOME" is not set)
endif

# set PLATFORM to either x64 or x86
ifndef PLATFORM
    UNAME:=$(shell uname -m)
    ifeq ($(UNAME),x86_64)
        PLATFORM:=x64
    else ifeq ($(UNAME),amd64)
        PLATFORM:=x64
    else
        PLATFORM:=x86
    endif
endif

CONFIG?=release
# set options for debug/release builds
ifeq ($(CONFIG),debug)
    CXXFLAGS+=-g
else ifeq ($(CONFIG),release)
    CXXFLAGS+=-O3
    LD_FLAGS+=-S
else
    $(error CONFIG must be 'release' or 'debug')
endif

VERSION=$(shell $(VOLAUVENT_HOME)/set_version)
VERSION_MAJOR=$(word 1,$(subst ., ,$(VERSION)))