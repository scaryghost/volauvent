include $(VOLAUVENT_HOME)/common.mk

# module, name, deps, inc dirs, lib dirs, static libs, dynamic libs
define define_app_rules =
$$(eval $$(call define_common_rules,$(1),$(2),$(4)))

$$($(1)_APP_OUTPUT): $$($(1)_OBJS) $$($(1)_MAIN_OBJ) $(3) | $$($(1)_OUTPUT_DIR)
	$(CXX) -o $$@ $$^ $$(addprefix -L,$(5)) -Wl,-Bstatic $$(addprefix -l,$(6)) -Wl,-Bdynamic $$(addprefix -l,$(7))
endef
