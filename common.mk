# module, app name, inc dirs
define define_common_rules =
.PHONY: $(1)-all $(1)-clean $(1)-cleaner $(1)-cleanest

$(1)_MAIN_SRC ?= $(shell cd $(1); grep -l "int main\(.*\)" -r -n *)
$(1)_CPP_SRCS := $(shell cd $(1); find * -name \*.cpp ! -wholename '$$($(1)_MAIN_SRC)')
$(1)_INC_DIRS := $(1) $(3)

$(1)_BUILD_DIR := $(1)/build
$(1)_OBJ_ROOT_DIR := $$($(1)_BUILD_DIR)/obj/$$(PLATFORM)/$$(CONFIG)
$(1)_OBJ_SRC_DIRS := $$(addprefix $$($(1)_OBJ_ROOT_DIR)/, $$(sort $$(dir $$($(1)_CPP_SRCS) $$($(1)_MAIN_SRC))))
$(1)_OUTPUT_DIR := $$($(1)_BUILD_DIR)/output/$$(PLATFORM)/$$(CONFIG)
$(1)_APP_OUTPUT := $$($(1)_OUTPUT_DIR)/$(2)

$(1)_OBJS := $$(addprefix $$($(1)_OBJ_ROOT_DIR)/,$$($(1)_CPP_SRCS:%.cpp=%.o))
$(1)_MAIN_OBJ := $$(addprefix $$($(1)_OBJ_ROOT_DIR)/,$$($(1)_MAIN_SRC:%.cpp=%.o))
$(1)_DEPS := $$($(1)_OBJS:%.o=%.d) $$($(1)_MAIN_OBJ:%.o=%.d)

-include $$($(1)_DEPS)

$(1)-all: $$($(1)_APP_OUTPUT)

$$($(1)_OBJ_SRC_DIRS) $$($(1)_OUTPUT_DIR):
	mkdir -p $$@

$$($(1)_OBJ_ROOT_DIR)/%.o: $1/%.cpp | $$($(1)_OBJ_SRC_DIRS)
	$(CXX) -MMD -MP -MF "$$(@:%.o=%.d)" -c -o $$@ $$($(1)_CXXFLAGS) $$(CXXFLAGS) $$(foreach header, $$($(1)_INC_DIRS), $$(addprefix -I,$$(header))) $$<

$(1)-clean:
	rm -Rf $$($(1)_OBJ_ROOT_DIR) $$($(1)_APP_OUTPUT)

$(1)-cleaner:
	rm -Rf $$($(1)_BUILD_DIR)/$(PLATFORM)/$(CONFIG) $$($(1)_APP_OUTPUT)

$(1)-cleanest:
	rm -Rf $$($(1)_BUILD_DIR)
endef